const problem1 = require("../problem1");
const path = require("path"); 

const samplefile_json = path.resolve(__dirname, "./Jsonfolder/samplefile.json")
const samplefile1_json = path.resolve(__dirname, "./Jsonfolder/samplefile1.json")
const samplefile2_json = path.resolve(__dirname, "./Jsonfolder/samplefile2.json")

const file = [samplefile_json,samplefile1_json,samplefile2_json]
const folder = path.resolve(__dirname, "./Jsonfolder")


problem1.operation(folder,file)


