const fs = require("fs")
const path = require("path")

const lipsum_txt = path.resolve(__dirname,"./lipsum.txt")
const upperCase_txt = path.resolve(__dirname, "./upperCase.txt")
const filenames_txt = path.resolve(__dirname, "./filesnames.txt")
const sentence_txt = path.resolve(__dirname, "./sentence.txt")
const sortData_txt = path.resolve(__dirname, "./sortData.txt");



function upperCase() {
    fs.readFile(lipsum_txt, "utf8", (err, data) => {
        data = data.toUpperCase();
        if (err) {
            console.log(err)
        } else {
            console.log(data)
        }
        
        fs.writeFile(upperCase_txt, data, (err) => {
            if (err) {
                console.log(err)
            }
            fs.appendFile(filenames_txt, "upperCase.txt\n", (err) => {
                if (err) {
                    console.log(err)
                }
                lowerCasesplit(upperCase_txt)
            })
        })   
    })
}



function lowerCasesplit(upperCase_txt) {
    fs.readFile(upperCase_txt,"utf8", (err, data) => {
        let dataarray = data.toLowerCase().split(".")
        if (err) {
            console.log(err)
        } else {
            console.log(data);
        }
        
        dataarray.forEach(element => {
            fs.appendFile(sentence_txt, element + " ", (err) => {
                if (err) {
                    console.log(err)
                }
            })

        });
        fs.appendFile(filenames_txt, "sentence.txt\n", (err)=>{
            if(err){
                console.log(err)
            }
            sortedData(sentence_txt)
        })
    }) 
}



function sortedData(sentence_txt) {
    fs.readFile(sentence_txt, "utf8", function (err, data) {
        let arrayofwords = data.replace(/,/g, '').replace(/\s+/g, ' ').split(' ');
        let sorted = arrayofwords.sort()
        sorted = sorted.join()
        fs.writeFile(sortData_txt,sorted,"utf8",(err)=>{
            if(err){
                console.log(err)
            }
            fs.appendFile(filenames_txt,"sortData.txt\n",(err)=>{
                if(err){
                    console.log(err)
                }
                deleteFile(filenames_txt)
            })
        })
    })
}

function deleteFile(filenames_txt){
    fs.readFile(filenames_txt,"utf8",(err,data)=>{
        let filesNamesArray =data.replace(/\s+/g, ' ').split(' ')
        filesNamesArray.forEach(element=>{
            if(element !=""){
                fs.unlink(element,(err)=>{
                    if(err){
                        console.log(err)
                    }  
                })
            }
        })
        console.log("ALlFiles Deleted")
    })
}








module.exports.upperCase=upperCase;
