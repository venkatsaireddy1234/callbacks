const fs = require("fs");
const path = require("path");


function makedir(path,file,callback) {
    fs.mkdir(path, (err) => {
        if (err) {
        console.log(err);
        } else {
        console.log("Jsonfolder created");
        callback(file, callback)
        }
        
    });
}


function creatingfiles(filePath, repeat) {
    for (let i = 0; i < filePath.length; i++) {
      fs.writeFile(filePath[i], "Hi", (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log(`File${i+1} created`);
          if (i ==0){
            repeat();
          }
        }
      });
    }
}
  
        
function deletefile(path){
    for (let i=0;i<path.length;i++){
        fs.unlink(path[i],err=>{
            if(err){
                console.log(err)
            }else{
                console.log(`JsonFile${i+1} deleted`)
            }
        })
    }
   
}          

                    
function operation(folder,file){
    makedir(folder,file,()=>{
       creatingfiles(file,()=>{
           deletefile(file)
       }) 
    })
}

  
module.exports.operation=operation;







